# README #

Implementation of a simple English-language learning game for elementary/middle school students. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words.

In this implementation, each key press (if it's a letter) will call a python function that checks if the current string input is a match with any of the vocabulary words. Using AJAX, the screen will automatically update when matches are made/when the required number of matches have been found.

## Author: Maxwell H Terry, mterry7@uoregon.edu #